VERSION ?= $(shell cat Cargo.toml | grep version | head -1 | cut -d ' ' -f 3 | sed s/\"//g)

dist: target/debian/gitlabfilesystem_$(VERSION)_amd64.deb

target/debug/gitlabfilesystem: $(wildcard src/*.rs)
	cargo build
target/release/gitlabfilesystem: $(wildcard src/*.rs)
	cargo build --release
target/debian/gitlabfilesystem_$(VERSION)_amd64.deb: target/release/gitlabfilesystem
	cargo deb

clean:
	cargo clean
	rm -f Cargo.lock
.PHONY: clean dist
