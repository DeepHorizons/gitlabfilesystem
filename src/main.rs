use std::env;
use std::path::Path;
use std::collections::HashMap;
use std::sync::Mutex;
use std::thread::JoinHandle;
use chrono::prelude::*;
use gitlab::models::GitlabJobModel;
use tokio::sync::mpsc::{self, Sender, Receiver};
use tracing::{trace, debug, info, warn};
use tracing_subscriber::{EnvFilter, fmt, prelude::*};
use std::convert::TryInto;
use zip::read::ZipArchive;
use std::io::Read;
use std::time;

mod gitlab;

#[derive(Debug)]
struct InodeData {
    attr: fuser::FileAttr,
    children: HashMap<String, u64>,
    //name: String,
    parent_inode: u64,
}

impl InodeData {
    fn new(attr: fuser::FileAttr, _name: String, parent_inode: u64) -> InodeData {
        let children = HashMap::new();
        InodeData { attr, children, parent_inode }
    }
}

struct GitlabFilesystem {
    msg_id: Mutex<u64>,
    client_send: Sender<gitlab::FsRequest>,
    client_recv: Receiver<gitlab::FsResponse>,
    response_queue: Vec<gitlab::FsResponse>,
    _client_thread: Option<JoinHandle<()>>,
    inode_to_inodedata: HashMap<u64, InodeData>,
    project_inode_to_gitlab_project: HashMap<u64, gitlab::models::GitlabProjectModel>,
    inode_to_gitlab_job_data: HashMap<u64, gitlab::models::GitlabJobProjectData>,
    last_project_update: DateTime<Local>,
}

impl GitlabFilesystem {
    fn new(gitlab_api_url: String, personal_access_token: String) -> GitlabFilesystem {
        let client = gitlab::Client::new(gitlab_api_url, personal_access_token);
        let inode_to_inodedata = HashMap::new();
        let project_inode_to_gitlab_project = HashMap::new();
        let inode_to_gitlab_job_data = HashMap::new();

        let (client_send, rx) = mpsc::channel(1024);
        let (tx, client_recv) = mpsc::channel(1024);

        debug!("Starting client thread");
        let _client_thread = std::thread::spawn(move || {
            gitlab::run_client(client, rx, tx)
        });
        let _client_thread = Some(_client_thread);

        let response_queue = Vec::new();
        let msg_id = 0;
        let msg_id = Mutex::new(msg_id);

        let mut fs = GitlabFilesystem { msg_id, client_send, client_recv, response_queue, _client_thread, inode_to_inodedata, project_inode_to_gitlab_project, inode_to_gitlab_job_data, last_project_update: Local::now() };

        // TODO run this multiple times when we shold update
        fs.sync_filesystem_to_gitlab();

        fs
    }

    fn get_or_block_on_msg(&mut self, msg_id: u64) -> gitlab::FsResponse {
        if let Some(pos) = self.response_queue.iter().position(|x| {
            match x {
                gitlab::FsResponse::Projects(id, _) => *id == msg_id,
                gitlab::FsResponse::Jobs(id, _ ) => *id == msg_id,
                gitlab::FsResponse::JobsMany(id, _ ) => *id == msg_id,
                gitlab::FsResponse::Artifact(id, _ ) => *id == msg_id,
            }
        }) {
            let msg = self.response_queue.remove(pos);
            return msg;
        };

        loop {
            let msg = self.client_recv.blocking_recv().unwrap();
            let does_match = match msg {
                gitlab::FsResponse::Projects(id, _) => id == msg_id,
                gitlab::FsResponse::Jobs(id, _ ) => id == msg_id,
                gitlab::FsResponse::JobsMany(id, _ ) => id == msg_id,
                gitlab::FsResponse::Artifact(id, _ ) => id == msg_id,
            };

            if does_match {
                return msg;
            } else {
                self.response_queue.push(msg);
            }
        }
    }

    fn sync_filesystem_to_gitlab(&mut self) {
        let mut id = self.msg_id.lock().unwrap();
        *id += 1;
        let id2 = *id;
        drop(id);
        let msg = gitlab::FsRequest::Projects(id2);
        self.client_send.try_send(msg).unwrap();
        let projects = self.get_or_block_on_msg(id2);
        let projects = match projects {
            gitlab::FsResponse::Projects(_, projects) => projects,
            _ => panic!("How did we get something else here?"),
        };
        self.create_filesystem(projects);
        trace!("inodes: {:#?}", self.inode_to_inodedata);
        self.last_project_update = Local::now();
    }

    fn max_inode_value(&mut self) -> u64 {
        *self.inode_to_inodedata.keys().max().unwrap()
    }

    fn add_path(&mut self, inode: u64, path: &std::path::Path, kind: fuser::FileType) -> u64 {
        trace!("add_path: inode={}, path={} kind={:?}", inode, path.to_str().unwrap(), kind);
        let ts = time::SystemTime::now();
        let mut inode_number = self.max_inode_value();

        // Starting with the given inode, see if the name is in .children
        let mut next_inode = inode;
        for component in path.components() {
            trace!("Component: {:?}", component);
            let comp_name = component.as_os_str().to_str().unwrap();
            trace!("inode: {}; next_inode: {}; Path: {}", inode, next_inode, comp_name);
            let inodedata = &mut self.inode_to_inodedata.get_mut(&next_inode).unwrap();

            // If the key exists use it, if not create it
            if inodedata.children.contains_key(comp_name) {
                next_inode = *inodedata.children.get(comp_name).unwrap();
                continue;
            } else {
                // Create a new directory, we can change it later
                let attr = fuser::FileAttr {
                    ino: inode_number + 1,
                    size: 0,
                    blocks: 0,
                    atime: ts,
                    mtime: ts,
                    ctime: ts,
                    crtime: ts,
                    kind: fuser::FileType::Directory,
                    perm: 0o555,
                    nlink: 0,
                    uid: 0,
                    gid: 0,
                    rdev: 0,
                    blksize: 0,
                    flags: 0,
                };
                inode_number += 1;  // Increment for the next round
                let proj_inodedata = InodeData::new(attr, (*comp_name).to_string().clone(), inode);

                // Add it to the current directory
                inodedata.children.insert((*comp_name).to_string().clone(), attr.ino);

                // Add it to the inodes
                self.inode_to_inodedata.insert(attr.ino, proj_inodedata);
                next_inode = attr.ino;
                continue;
            }
        }

        // Modify the last entry to be kind
        let inodedata = self.inode_to_inodedata.get_mut(&next_inode).unwrap();
        inodedata.attr.kind = kind;

        // return the last inode
        next_inode
    }

    fn create_layout(&mut self, artifact_folder_inode: u64, project: &gitlab::models::GitlabProjectModel, jobs: &Vec<gitlab::models::GitlabJobModel>, top_folder_name: &std::path::Path, artifact_path_fn: impl Fn(&gitlab::models::GitlabJobModel, &gitlab::models::GitlabJobArtifactsFileModel) -> String) {
        let inode = self.add_path(artifact_folder_inode, top_folder_name, fuser::FileType::Directory);

        for job in jobs {
            // Some successful jobs dont have artifacts, skip them
            let artifacts_file = match &job.artifacts_file {
                Some(artifacts_file) => artifacts_file,
                None => {
                    continue;
                }
            };

            // Where are we supposed to put the artifact file
            let p = artifact_path_fn(job, artifacts_file);
            let p = std::path::Path::new(&p);

            let artifact_inode = self.add_path(inode, p, fuser::FileType::RegularFile);

            // Create a directory of the same name as the artifact
            let p_parent = &p.parent().unwrap();
            let p_stem = &p.file_stem().unwrap();
            let p_dir = p_parent.join(p_stem);
            let artifact_dir_inode = self.add_path(inode, &p_dir, fuser::FileType::Directory);

            // Let us know the job id for the inode for when we look it up
            self.inode_to_gitlab_job_data.insert(artifact_inode, gitlab::models::GitlabJobProjectData { job_id: job.id, project_id: project.id, file: None });
            self.inode_to_gitlab_job_data.insert(artifact_dir_inode, gitlab::models::GitlabJobProjectData { job_id: job.id, project_id: project.id, file: None});

            // update the artifact_inode attrs, size
            let artifact_inodedata = &mut self.inode_to_inodedata.get_mut(&artifact_inode).unwrap();
            artifact_inodedata.attr.size = artifacts_file.size;
        }
    }

    fn create_by_job_id_layout(&mut self, artifact_folder_inode: u64, project: &gitlab::models::GitlabProjectModel, jobs: &Vec<gitlab::models::GitlabJobModel>) {
        let root_path = std::path::Path::new("by-job-id");
        let artifact_name = |job: &gitlab::models::GitlabJobModel, artifact: &gitlab::models::GitlabJobArtifactsFileModel| { format!("{}/{}", &job.id, artifact.filename) };
        self.create_layout(artifact_folder_inode, project, jobs, root_path, artifact_name);
    }

    fn create_by_ref_layout(&mut self, artifact_folder_inode: u64, project: &gitlab::models::GitlabProjectModel, jobs: &Vec<gitlab::models::GitlabJobModel>) {
        let root_path = std::path::Path::new("by-ref");
        let artifact_name = |job: &gitlab::models::GitlabJobModel, artifact: &gitlab::models::GitlabJobArtifactsFileModel| { format!("{}/{}/{}", &job._ref, job.name, artifact.filename) };
        self.create_layout(artifact_folder_inode, project, jobs, root_path, artifact_name);
    }

    fn create_by_pipeline_layout(&mut self, artifact_folder_inode: u64, project: &gitlab::models::GitlabProjectModel, jobs: &Vec<gitlab::models::GitlabJobModel>) {
        let root_path = std::path::Path::new("by-pipeline");
        let artifact_name = |job: &gitlab::models::GitlabJobModel, artifact: &gitlab::models::GitlabJobArtifactsFileModel| { format!("{}/{}/{}", &job.pipeline.id, job.name, artifact.filename) };
        self.create_layout(artifact_folder_inode, project, jobs, root_path, artifact_name);
    }

    fn create_project_layout2(&mut self, project_inode: u64, project: &gitlab::models::GitlabProjectModel, jobs: Vec<GitlabJobModel>) {
        info!("Creating project layout for {}", project.name);
        // .../<project>/artifact
        let artifact_folder_inode = self.add_path(project_inode, std::path::Path::new("artifact"), fuser::FileType::Directory);

        // artifact/by-job-id/<job id>/artifact.zip
        self.create_by_job_id_layout(artifact_folder_inode, project, &jobs);
        
        // artifact/by-ref/<ref>/<job name>/artifact.zip
        self.create_by_ref_layout(artifact_folder_inode, project, &jobs);

        // artifact/by-pipeline/<pipeline number>/<job name>/artifact.zip
        self.create_by_pipeline_layout(artifact_folder_inode, project, &jobs);
    }

    fn create_project_layout(&mut self, project_inode: u64, project: &gitlab::models::GitlabProjectModel) {
        info!("Creating project layout for {}", project.name);
        // .../<project>/artifact
        let artifact_folder_inode = self.add_path(project_inode, std::path::Path::new("artifact"), fuser::FileType::Directory);

        // Get all the jobs for the project once
        let mut id = self.msg_id.lock().unwrap();
        *id += 1;
        let id2 = *id;
        drop(id);
        let msg = gitlab::FsRequest::Jobs(id2, project.clone());
        self.client_send.blocking_send(msg).unwrap();
        let jobs = self.get_or_block_on_msg(id2);
        let jobs = match jobs {
            gitlab::FsResponse::Jobs(_, jobs) => jobs,
            _ => panic!("How did we get something else here?"),
        };

        // artifact/by-job-id/<job id>/artifact.zip
        self.create_by_job_id_layout(artifact_folder_inode, project, &jobs);
        
        // artifact/by-ref/<ref>/<job name>/artifact.zip
        self.create_by_ref_layout(artifact_folder_inode, project, &jobs);

        // artifact/by-pipeline/<pipeline number>/<job name>/artifact.zip
        self.create_by_pipeline_layout(artifact_folder_inode, project, &jobs);
    }

    /// Look throgh the projects and create the filesystem structure needed
    fn create_filesystem(&mut self, projects: Vec<gitlab::models::GitlabProjectModel>) {
        // TODO see if the item exists already, if they do skip? still need to do all jobs for each
        // project
        // TODO use the time the filesystem was mounted for mtime and ctime.
        let ts = time::SystemTime::now();
        let root_inode = 1;
        let parent_inode = 0;

        // Create the root directory
        // Start at 1 for the root dir
        let attr = fuser::FileAttr {
            ino: root_inode,
            size: 0,
            blocks: 0,
            atime: ts,
            mtime: ts,
            ctime: ts,
            crtime: ts,
            kind: fuser::FileType::Directory,
            perm: 0o555,
            nlink: 0,
            uid: 0,
            gid: 0,
            rdev: 0,
            blksize: 0,
            flags: 0,
        };
        let name = "/".to_string();
        let root_inodedata = InodeData::new(attr, name, parent_inode);

        // Maual test to see if we can get a folder and how it behaves
        //let attr = fuser::FileAttr {
        //    ino: 1000,
        //    size: 0,
        //    blocks: 0,
        //    atime: ts,
        //    mtime: ts,
        //    ctime: ts,
        //    crtime: ts,
        //    kind: fuser::FileType::Directory,
        //    perm: 0o555,
        //    nlink: 0,
        //    uid: 0,
        //    gid: 0,
        //    rdev: 0,
        //    flags: 0,
        //};
        //let name = "test".to_string();
        //let test_inodedata = InodeData::new(attr, name.clone(), root_inodedata.attr.ino);

        // Add it to the root directory
        //root_inodedata.children.insert(name, attr.ino);
        // Add the inode data to the map
        //self.inode_to_inodedata.insert(attr.ino, test_inodedata);
        self.inode_to_inodedata.insert(root_inodedata.attr.ino, root_inodedata);  // This needs to come after adding the child because of borrowing
        
        info!("Creating all project layouts");
        let mut id = self.msg_id.lock().unwrap();
        *id += 1;
        let id2 = *id;
        drop(id);
        let msg = gitlab::FsRequest::JobsMany(id2, projects);
        self.client_send.blocking_send(msg).unwrap();
        let r = self.get_or_block_on_msg(id2);
        let r = match r {
            gitlab::FsResponse::JobsMany(_,r ) => r,
            _ => panic!("How did we get here?"),
        };

        // for every message submitted create a directory structure
        for (project, jobs) in r {
            let path = std::path::Path::new(&project.path_with_namespace);
            let project_inode = self.add_path(1, path, fuser::FileType::Directory);
            // TODO Create the artifact structure
            self.create_project_layout2(project_inode, &project, jobs);

            // Say this inode is related to this project
            self.project_inode_to_gitlab_project.insert(project_inode, project.clone());
        }
    }

    /// Get the project inode if this branch is under one
    fn has_associated_project_inode(&self, inodedata: &InodeData) -> Option<u64> {
        if inodedata.attr.ino <= 1 {
            return None;
        }

        if let Some(_project) = self.project_inode_to_gitlab_project.get(&inodedata.attr.ino) {
            Some(inodedata.attr.ino)
        } else {
            let parent_inodedata = self.inode_to_inodedata.get(&inodedata.parent_inode).unwrap();
            self.has_associated_project_inode(parent_inodedata)
        }
    }

    /// Used when creating virtual files from zip binaries
    fn update_and_find(&mut self, parent: u64, name: &str) -> Option<u64> {
        // Didnt find anything, is there a gitlab file attached to it?
        // TODO turn this into a function
        let mut map: HashMap<String, (fuser::FileType, gitlab::models::GitlabJobProjectData, bytes::Bytes, u64)> = HashMap::new();
        // If there is job data attached to this, add it to the path
        let jobdata = self.inode_to_gitlab_job_data.get(&parent)?;
        let jobdata = jobdata.clone(); // XXX fixes a borrow issue

        // Found job data, read the zip
        // Make the network request
        debug!("Starting artifact request...");
        let mut id = self.msg_id.lock().unwrap();
        *id += 1;
        let id2 = *id;
        drop(id);
        let msg = gitlab::FsRequest::Artifact(id2, jobdata.clone());
        self.client_send.blocking_send(msg).unwrap();
        let data = self.get_or_block_on_msg(id2);
        let data = match data {
            gitlab::FsResponse::Artifact(_, b) => b,
            _ => panic!("How did we get something else here?"),
        };
        debug!("done with request, size: {}", data.len());
        // TODO get zip listing, add children
        let reader = std::io::Cursor::new(data.clone());
        let mut zip = ZipArchive::new(reader).unwrap();

        // Go through the zip and get all the entries
        for i in 0..zip.len() {
            let entry = zip.by_index(i).unwrap();
            let size = entry.size();
            let zipname = entry.name().to_owned();
            debug!("Zip entry: {}", zipname);

            // Add the entry and get an inode
            // TODO avoid the clones? the data clone doesnt matter so much because its
            // Bytes which is an ref count
            if entry.is_file() {
                map.insert(zipname.to_string(), (fuser::FileType::RegularFile, jobdata.to_owned(), data.clone(), size.to_owned()));
            } else {
                // TODO do we need to clone the data for a directory?
                map.insert(zipname.to_string(), (fuser::FileType::Directory, jobdata.clone(), data.clone(), size.to_owned()));
            };
        }

        let mut name_inode = None;
        // TODO why does this need to come after the above block? It was complaining about the self
        // mutability
        for (k, v) in map {
            let (t, jobdata, data, size) = v;
            let p = Path::new(&k);
            debug!("Entry: {}", p.to_str().unwrap());

            // Add the entry and get an inode
            let m = gitlab::models::GitlabJobProjectData { job_id: jobdata.job_id, project_id: jobdata.project_id, file: Some(gitlab::models::ZipFile { path: Some(k.to_owned()), data }) };
            let inode = self.add_path(parent, p, t.to_owned());
            self.inode_to_gitlab_job_data.insert(inode, m); // The path has access to the file, TODO need path in zip to get the data...

            // update the artifact_inode attrs, size
            let inodedata = &mut self.inode_to_inodedata.get_mut(&inode).unwrap();
            inodedata.attr.size = size;
            // If we match what we are looking for, add it to the reply
            if p.to_str().unwrap() == name {
                name_inode = Some(inode);
            }
        }
        name_inode
    }
}

impl fuser::Filesystem for GitlabFilesystem {
    fn read(&mut self,
        _req: &fuser::Request,
        ino: u64,
        fh: u64,
        offset: i64,
        size: u32,
        _flags: i32,
        _lock_owner: Option<u64>,
        reply: fuser::ReplyData
        ) {
        debug!("read(ino={}, fh={}, offset={}, size={})", ino, fh, offset, size);

        // See if the file is in the inode_to_gitlab_job_id
        // If it is, get the file
        // if not error
        match self.inode_to_gitlab_job_data.get(&ino) {
            Some(jobdata) => {
                // If we already have a file associated with it, get it
                let data = if let Some(file) = &jobdata.file {
                    // If there is a path, read it like a zip file and get that path. Otherwise just
                    // return the data
                    let d = if let Some(path) = &file.path {
                        debug!("Getting data from zip");
                        let reader = std::io::Cursor::new(file.data.clone());
                        let mut zip = ZipArchive::new(reader).unwrap();
                        let mut f = zip.by_name(path).unwrap();
                        let mut buffer = Vec::new();
                        let _buffer_len = f.read_to_end(&mut buffer).unwrap();
                        bytes::Bytes::from(buffer)
                    } else {
                        debug!("getting raw bytes");
                        file.data.clone()
                    };
                    d
                } else {
                    panic!("Lets see if we get to this code path...");
                };

                debug!("bytes len: {}", data.len());

                // If data > size, we need to return slices
                // If the offset is more than the end of the file, the slice will start at the end
                let start: usize = if offset >= data.len().try_into().unwrap() {
                    data.len()
                } else {
                    offset.try_into().unwrap()
                };

                let offset_end = offset as u32 + size;
                // If the size of the slice is more than the end, the slice will end at the end
                let end: usize = if offset_end >= data.len() as u32 {
                    data.len()
                } else {
                    offset_end as usize
                };

                debug!("Returining data with slice: [{}..{}]", start, end);
                reply.data(&data[start..end]);
            },
            None => {
                reply.error(libc::ENOENT);
            },
        }
    }

    fn lookup(&mut self, _req: &fuser::Request, parent: u64, name: &std::ffi::OsStr, reply: fuser::ReplyEntry) {
        let name = name.to_str().unwrap();
        debug!("lookup(parent={}, name={})", parent, name);

        // XXX TODO cant make this async, how to do that?
        //let name_inode = self.update_and_find_async(parent, name);
        //let name_inode = self.rt.block_on(name_inode);
        let name_inode = self.update_and_find(parent, name);

        // lookup the parent inode
        let parent_inodedata = match self.inode_to_inodedata.get(&parent) {
            Some(inodedata) => inodedata,
            None => {
                debug!("lookup: Could not find parent inode, which is weird");
                reply.error(libc::ENOENT);
                return;
            },
        };

        // Search the childern for the name, return that attr
        if let Some(inode) = parent_inodedata.children.get(name) {
            debug!("lookup: Found a child");
            let inodedata = self.inode_to_inodedata.get(inode).unwrap();

            // Return the attrs
            let ttl = time::Duration::new(1,0);
            reply.entry(&ttl, &inodedata.attr, 0);
            return
        }

        // Didnt find anything, is there a gitlab file attached to it?
        // TODO turn this into a function
        // See if we have an inode, which means we found it
        match name_inode {
            Some(inode) => {
                let inodedata = self.inode_to_inodedata.get(&inode).unwrap();

                // Return the attrs
                let ttl = time::Duration::new(1,0);
                reply.entry(&ttl, &inodedata.attr, 0);
            },
            None => {
                // gitlab file found, but the path isnt in it
                warn!("lookup: Could not find the path: {}", name);
                reply.error(libc::ENOENT);
            },
        }
    }

    fn getattr(&mut self, _req: &fuser::Request, ino: u64, _fh: Option<u64>, reply: fuser::ReplyAttr) {
        info!("getattr(ino={})", ino);

        // get the given inodedata addr
        let inode_attr = match self.inode_to_inodedata.get(&ino) {
            Some(inodedata) => inodedata.attr,
            None => {
                reply.error(libc::ENOENT);
                return;
            }
        };

        let ttl = time::Duration::new(1, 0);
        reply.attr(&ttl, &inode_attr);
    }

    // TODO fails if it has not downloaded the folder yet. If the folder does not exist but matches
    // to a known zip folder then get it and continue
    fn readdir(&mut self, _req: &fuser::Request, ino: u64, fh: u64, offset: i64, mut reply: fuser::ReplyDirectory) {
        info!("readdir(ino={}, fh={}, offset={})", ino, fh, offset);
        let name = "";
        // XXX TODO cant make this async but it needs to be, how to do that?
        //let _data = self.update_and_find_async(ino, name);
        //let _data = self.rt.block_on(_data);
        let _data = self.update_and_find(ino, name);

        // get the InodeData for the ino
        let inodedata = match self.inode_to_inodedata.get(&ino) {
            Some(inodedata) => inodedata,
            None => {
                reply.error(libc::ENOENT);
                return;
            },
        };

        let mut total_offset: i64 = 1_i64;

        // TODO if offset is != 0, what do I do?
        // add the entries from the inos children
        if offset == 0 {
            // Add the . and .. directories
            let _ = reply.add(inodedata.attr.ino, 0, fuser::FileType::Directory, Path::new("."));
            let _ = reply.add(inodedata.parent_inode, 1, fuser::FileType::Directory, Path::new(".."));

            // No job data, look for children we added
            // Add every child
            for (path, child_inode) in inodedata.children.iter() {
                total_offset += 1;
                let child_inodedata = self.inode_to_inodedata.get(child_inode).unwrap();
                let _ = reply.add(child_inodedata.attr.ino, total_offset, child_inodedata.attr.kind, path);
            }
        }

        // Get the latest data
        // XXX Needs to be at the end because of the mutable borrow
        if let Some(project_inode) = self.has_associated_project_inode(inodedata) {
            debug!("Associated project inode: {:#?}", project_inode);
            let project = &self.project_inode_to_gitlab_project.get(&project_inode).unwrap().clone();

            self.create_project_layout(project_inode, project)

        }

        reply.ok();
    }

    fn destroy(&mut self) {
        debug!("Sending Close request to client");
        let msg = gitlab::FsRequest::Close;
        self.client_send.try_send(msg).unwrap();
    }
}


fn main() {
    tracing_subscriber::registry()
        .with(fmt::layer().with_line_number(true))
        .with(EnvFilter::from_default_env())
        .init();

    // TODO move to clap
    // Get arguments
    let (pat, url) = match env::args().nth(1) {
        Some(gitlab_ident) => {
            let gi: Vec<&str> = gitlab_ident.split('@').collect();
            (gi[0].to_owned(), gi[1].to_owned())
        },
        None => {
            println!("Error with gitlab identity; Usage: {0} <gitlab PAT>:<gitlab api url> <MOUNTPOINT>\n{0} abc123@https://gitlab.com/api/v4 /mnt", env::args().next().unwrap());
            return;
        }
    };

    let mountpoint = match env::args().nth(2) {
        Some(path) => path,
        None => {
            println!("Error getting mount point; Usage: {0} <gitlab PAT>:<gitlab api url> <MOUNTPOINT>\n{0} abc123@https://gitlab.com/api/v4 /mnt", env::args().next().unwrap());
            return;
        }
    };

    // XXX dont print the whole pat as a security precaution
    //debug!("pat='{}', url='{}'", pat, url);
    info!("Creating filesystem...");    
    let fs = GitlabFilesystem::new(url, pat);
    info!("Mounting filesytem...");
    let res = fuser::mount2(fs, &mountpoint, &[]);
    match res {
        Ok(_) => {},
        Err(e) => panic!("Error with mount: {e:?}"),
    }
    // TODO how to kill the client?
    info!("done");
}

