pub mod models;

use tokio::sync::mpsc::{Receiver, Sender};
use tokio::sync::Mutex;
use tokio::task::{self, JoinError};
use tracing::{trace, debug, info, warn, error};
use reqwest::header;
use bytes::Bytes;
use serde::{ Deserialize, Serialize };
use std::collections::HashMap;
use std::future::Future;
use std::pin::Pin;
use std::sync::{Arc};
use chrono::prelude::*;
use std::time::{Duration, Instant};
use std::error::Error;
use futures::stream::StreamExt;


const CONCURRENT_CONNECTIONS: usize = 10;


// TODO how to make these two enums one?
#[derive(Debug)]
pub enum FsRequest {
    Close,
    Projects(u64),
    Jobs(u64, models::GitlabProjectModel),
    JobsMany(u64, Vec<models::GitlabProjectModel>),
    Artifact(u64, models::GitlabJobProjectData),
}

#[derive(Debug)]
pub enum FsResponse {
    Projects(u64, Vec<models::GitlabProjectModel>),
    Jobs(u64, Vec<models::GitlabJobModel>),
    JobsMany(u64, Vec<(models::GitlabProjectModel, Vec<models::GitlabJobModel>)>),
    Artifact(u64, Bytes),
}

struct Cache<T: Send> {
    valid_until: Instant,
    value: T,
    req: Option<OnGoingRequest>,
}

impl<T: Send> Cache<T> {
    fn new(duration: Duration, value: T, req: Option<OnGoingRequest>) -> Self {
       let valid_until = Instant::now() + duration;
       Cache { valid_until, value, req}
    }

    /// Cache for a short while, seconds
    fn quick_duration(value: T, req: Option<OnGoingRequest>) -> Self {
        Cache::new(Duration::from_secs(2), value, req)
    }

    /// Cache for a long time, hour
    fn long_duration(value: T, req: Option<OnGoingRequest>) -> Self {
        Cache::new(Duration::from_secs(3600), value, req)
    }
}

/// Represents a request from the server and a channel to get the data from
struct OnGoingRequest{
    f: Pin<Box<dyn Future<Output = Result<reqwest::Response, JoinError>> + Send>>,
}

pub struct Client {
    gitlab_api_url: String,
    client_async: reqwest::Client,
    ratelimit_remaining: Option<u32>,
    ratelimit_resettime: Option<DateTime<Utc>>,
    // XXX Cant have a generic cache because Response is not copyable
    //api_cache: HashMap<String, Cache<reqwest::Response>>,
    // Do a cache for JSON and bytes
    // TODO save the value as a serde_json::Value or Map so we are not constantly serializeing it,
    // but we need to be able to make a clone of it for ownweship...
    json_cache: HashMap<String, Cache<String>>,
    bytes_cache: HashMap<String, Cache<Bytes>>,
}

impl Client {
    pub fn new(gitlab_api_url: String, personal_access_token: String) -> Client {
        let mut headers = reqwest::header::HeaderMap::new();
        headers.insert("Private-Token", header::HeaderValue::from_str(&personal_access_token).unwrap());
        let client_async = reqwest::Client::builder()
            .default_headers(headers)
            .timeout(Duration::from_secs(5))
            .build()
            .unwrap();

        let json_cache = HashMap::new();
        let bytes_cache = HashMap::new();

        Client { gitlab_api_url, client_async, ratelimit_remaining: None, ratelimit_resettime: None, json_cache, bytes_cache }
    }

    /// Update the rate limit values
    /// This should be run after getting a request
    fn update_response_rate_limit(&mut self, headers: &reqwest::header::HeaderMap) {
        debug!("Updating RateLimit...");
        debug!("{headers:#?}");
        if let Some(headervalue) = headers.get("RateLimit-Remaining") {
            if let Ok(remaining_str) = headervalue.to_str() {
                if let Ok(remaining) = remaining_str.parse() {
                    debug!("RateLimit Remaining: '{remaining}'");
                    self.ratelimit_remaining = Some(remaining);
                }
            }
        }

        if let Some(headervalue) = headers.get("RateLimit-Reset") {
            if let Ok(reset_str) = headervalue.to_str() {
                if let Ok(reset) = reset_str.parse() {
                    let t = Utc.timestamp_opt(reset, 0).unwrap();
                    let local = t.naive_local();
                    debug!("RateLimit Reset: '{t}' :: '{local}'");
                    self.ratelimit_resettime = Some(t);
                }
            }
        }
    }

    /// See if we need to sleep before making a request
    ///
    /// # Example
    /// ```
    /// if let Some(duration) = self.should_rate_limit() {
    ///     std::thread::sleep(duration);
    /// }
    /// ```
    fn should_rate_limit(&self, num_reqs: Option<u32>) -> Option<Duration> {
        match self.ratelimit_remaining {
            Some(remaining) => {
                // Get the time from now until the ratelimit resets
                let duration = self.ratelimit_resettime.unwrap().signed_duration_since(Utc::now());
                let num_reqs = num_reqs.unwrap_or(1);

                if remaining == 0 {
                    // We ran out of requests and need to wait the whole time
                    Some(duration.to_std().unwrap())
                } else if num_reqs > remaining {
                    // We want to send to many requests
                    Some(duration.to_std().unwrap())
                } else if (remaining - num_reqs) < 200 {
                    // We have used some reqeusts and should start sleeping
                    // Get an evenly distribution of equal parts
                    let subset = duration / (remaining - num_reqs) as i32;

                    Some(subset.to_std().unwrap())
                } else {
                    // No need to sleep since we are not near the limit
                    None
                }
            },
            // No need to wait if we never made a request
            None => None,
        }
    }

    async fn make_call_many_without_update(&self, urls: Vec<String>) -> Vec<reqwest::Response> {
        // Make sure we dont overload the ratelimit with a bunch of requests
        let urls_len = urls.len() as u32;
        if let Some(duration) = self.should_rate_limit(Some(urls_len)) {
            debug!("Sleeping to stay inside rate limit: {:#?}", duration);
            tokio::time::sleep(duration).await;
            debug!("Done sleeping");
        }

        let resps = futures::stream::iter(urls).map(|url| {
            let url = self.gitlab_api_url.to_owned() + &url;

            async move {
                let mut n = 3;
                loop {
                    debug!("Try '{n}'");
                    let resp = self.client_async.get(&url)
                       .send()
                       .await;
                    match resp {
                        Ok(resp) => break resp,
                        Err(e) => {
                           error!("PRE HTTP status error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                           if n > 0 {
                               n -= 1;
                           } else {
                               panic!("{e}");
                           }
                        },
                    };
                }
            }
        }).buffer_unordered(CONCURRENT_CONNECTIONS);

        let resps: Vec<_> = resps.collect().await;
        resps
    }

    async fn make_call_many(&mut self, urls: Vec<String>) -> Vec<reqwest::Response> {
        let resps = self.make_call_many_without_update(urls).await;

        // TODO get the last response and update based off of that, this is just temporary
        let resp = resps.last().unwrap();
        self.update_response_rate_limit(resp.headers());
        resps
    }

    async fn make_call_without_update(&self, url: String) -> reqwest::Response {
        if let Some(duration) = self.should_rate_limit(None) {
            debug!("Sleeping to stay inside rate limit: {:#?}", duration);
            tokio::time::sleep(duration).await;
            debug!("Done sleeping");
        }

        let url = self.gitlab_api_url.to_owned() + &url;

        let mut n = 3;
        loop {
            debug!("Try '{n}'");
            let resp = self.client_async.get(&url)
               .send()
               .await;
            match resp {
                Ok(resp) => break resp,
                Err(e) => {
                   error!("PRE HTTP status error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                   if n > 0 {
                       n -= 1;
                   } else {
                       panic!("{e}");
                   }
                },
            };
        }
    }

    async fn make_call(&mut self, url: String) -> reqwest::Response {
        let resp = self.make_call_without_update(url).await;
        self.update_response_rate_limit(resp.headers());

        resp
    }
    
    // TODO return an option so if it fails we can say we dont have anything for that project, like
    // if the project has CI disabled like I just tested...
    async fn make_call_to_json_many_async<T: std::marker::Sync + std::marker::Send + Serialize + std::fmt::Debug + for<'de> Deserialize<'de> + Clone> (&mut self, urls: Vec<String>) -> Vec<(String, T)> {
        let resps = self.make_call_many(urls).await;
        let mut r = Vec::new();

        for resp in resps {
            let url = resp.url().to_string();
            let resp = match resp.error_for_status() {
                Ok(r) => r,
                Err(e) => {
                   error!("HTTP status error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                   panic!("{e}"); 
                },
            };

            let json = match resp.json().await {
                Ok(j) => j,
                Err(e) => {
                    error!("JSON decode error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                    panic!("{e}");
                },
            };
            trace!("json: {:#?}", json);

            // TODO borrow issue here...
            //let req = self.make_call_without_update(url.to_owned());
            //let req = task::spawn_local(req);
            //let req = Box::pin(req);
            //let req = OnGoingRequest { f: req };
            //let req = Some(req);
            let cache = Cache::quick_duration(serde_json::to_string(&json).unwrap(), None);
            self.json_cache.insert(url.clone(), cache);

            r.push((url, json));
        }

        r
    }

    // TODO return an option so if it fails we can say we dont have anything for that project, like
    // if the project has CI disabled like I just tested...
    async fn make_call_to_json_async<T: std::marker::Sync + std::marker::Send + Serialize + std::fmt::Debug + for<'de> Deserialize<'de> + Clone> (&mut self, url: &str) -> T {
        let resp = match self.json_cache.get_mut(url) {
            Some(cache) => {
                if Instant::now() < cache.valid_until {
                    debug!("Return JSON cache for: '{url}'");
                    return serde_json::from_str(&cache.value).unwrap();
                    // TODO start new request if valid_until is 1/2 complete?
                }

                if let Some(req) = &mut cache.req {
                    debug!("Waiting for ongoing request");
                    let r = req.f.as_mut();
                    let r = r.await;
                    debug!("Got response");
                    r.unwrap() 
                } else {
                    warn!("Outdated cache with no ongoing request. Starting request");
                    let r = self.make_call(url.to_string()).await;
                    debug!("Got response");
                    r
                }
            },
            None => {
                debug!("No cache, starting request");
                let r = self.make_call(url.to_string()).await;
                debug!("Got response");
                r
            },
        };

        let resp = match resp.error_for_status() {
            Ok(r) => r,
            Err(e) => {
               error!("HTTP status error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
               panic!("{e}"); 
            },
        };

        let json = match resp.json().await {
            Ok(j) => j,
            Err(e) => {
                error!("JSON decode error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                panic!("{e}");
            },
        };
        trace!("json: {:#?}", json);

        // TODO borrow issue here...
        //let req = self.make_call_without_update(url.to_owned());
        //let req = task::spawn_local(req);
        //let req = Box::pin(req);
        //let req = OnGoingRequest { f: req };
        //let req = Some(req);
        let cache = Cache::quick_duration(serde_json::to_string(&json).unwrap(), None);
        self.json_cache.insert(url.to_owned(), cache);

        json
    }

    async fn make_call_to_bytes_async(&mut self, url: &str) -> Bytes {
        let resp = match self.bytes_cache.get_mut(url) {
            Some(cache) => {
                if Instant::now() < cache.valid_until {
                    debug!("Return Byte cache for: '{url}'");
                    return cache.value.clone()
                }
                
                if let Some(req) = &mut cache.req {
                    debug!("Waiting for ongoing request...");
                    let r = req.f.as_mut();
                    let r = r.await.unwrap();
                    debug!("Got response");
                    r 
                } else {
                    warn!("Outdated cache with no ongoing request. Starting request");
                    self.make_call(url.to_string()).await
                }
            },
            None => {
                debug!("No cache, starting request");
                self.make_call(url.to_string()).await
            },
        };
        
        let resp = match resp.error_for_status() {
            Ok(r) => r,
            Err(e) => {
               error!("HTTP status error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
               panic!("{e}"); 
            },
        };
        
        let data = match resp.bytes().await {
            Ok(b) => b,
            Err(e) => {
                error!("Bytes decode error: {e:?}; URL: '{:?}'; Status: '{:?}'; Source: '{:?}'", e.url(), e.status(), e.source());
                panic!("{e}");
            },
        };
        trace!("data: {:#?}", data);
        
        // The request finished, update the cache and return this value
        // Dont start another request since it will be a long cache
        // Add the value to the cache
        // TODO maybe done clone and return a reference?
        let cache = Cache::long_duration(data.clone(), None);
        self.bytes_cache.insert(url.to_owned(), cache);

        data
    }

    pub async fn get_projects_async(&mut self) -> Vec<models::GitlabProjectModel> {
        info!("Getting projects...");
        let mut v: Vec<_> = self.make_call_to_json_async("/projects?membership=true&per_page=100").await;
        // TODO apparently there is a header that says how many pages there are
        if v.len() == 100 {
            info!("Got 100 projects, getting multiple pages...");
            let mut i = 2;
            loop {
                info!("Getting page {i}");
                let mut v2: Vec<_> = self.make_call_to_json_async(&format!("/projects?membership=true&per_page=100&page={i}")).await;
                let v2_len = v2.len();
                debug!("Got {v2_len} results");
                v.append(&mut v2);
                if v2_len < 100 {
                    info!("Got all projects");
                    break;
                }
                i += 1;
            }
        }
        v
    }

    pub async fn get_jobs_async(&mut self, project: models::GitlabProjectModel) -> Vec<models::GitlabJobModel> {
        info!("Getting jobs...");
        let url = "/projects/".to_owned() + &project.id.to_string() + "/jobs?scope=success";
        let jobs: Vec<models::GitlabJobModel> = self.make_call_to_json_async(&url).await;
        jobs
    }
    
    pub async fn get_many_jobs_async(&mut self, projects: Vec<models::GitlabProjectModel>) -> Vec<(models::GitlabProjectModel, Vec<models::GitlabJobModel>)> {
        info!("Getting many jobs...");
        let urls: Vec<_> = projects
            .iter()
            .map(|project| "/projects/".to_owned() + &project.id.to_string() + "/jobs?scope=success")
            .collect();
        let resps: Vec<(_, Vec<models::GitlabJobModel>)> = self.make_call_to_json_many_async(urls).await;
        let mut r = Vec::new();
        for (url, jobs) in resps {
            let index = url.find("/projects/").unwrap() + 10;
            let index2 = url.find("/jobs?scope=success").unwrap();
            let project_id = url[index..index2].to_owned();
            trace!("Extracting URL '{url}'; index1: {index}; index2: {index2}; project_id: '{project_id}'");
            let project_id: i64 = project_id.parse().unwrap();
            for project in projects.iter() {
                if project.id == project_id {
                    r.push((project.to_owned(), jobs));
                    break;
                }
            }
        }
        r
    }

    pub async fn get_artifact_from_job_id_async(&mut self, project_id: i64, job_id: u64) -> Bytes {
        info!("Getting artifact...");
        self.make_call_to_bytes_async(&("/projects/".to_owned() + &project_id.to_string() + "/jobs/" + &job_id.to_string() + "/artifacts")).await
    }
}


pub fn run_client(client: Client, mut rx: Receiver<FsRequest>, tx: Sender<FsResponse>) {
    info!("Starting client executor");
    tokio_uring::start(async move {
        info!("Started client executor");
        let local = task::LocalSet::new();

        local.run_until(async move {
            let v = futures::stream::futures_unordered::FuturesUnordered::new();
            let client = Mutex::new(client);
            let client = Arc::new(client);
            info!("Starting client loop");
            loop {
                tokio::select! {
                    msg = rx.recv() => {
                        debug!("Got msg: {msg:?}");
                        match msg {
                            None => {
                                info!("Client channel got None, closing");
                                return;
                            },
                            Some(FsRequest::Close) => {
                                info!("Client channel got Close, closing");
                                return;
                            },
                            Some(FsRequest::Projects(id)) => {
                                debug!("Got project request");
                                let tx2 = tx.clone();
                                let client2 = client.clone();
                                let f = async move {
                                    debug!("Getting lock...");
                                    //let mut client2 = client2.lock().await;
                                    debug!("Getting projects...");
                                    let mut client2 = client2.lock().await;
                                    let projects = client2.get_projects_async().await;
                                    drop(client2);
                                    debug!("Sending project msg...");
                                    let msg = FsResponse::Projects(id, projects.to_owned());
                                    tx2.send(msg).await.unwrap();
                                    debug!("Send project msg");
                                };
                                let f = task::spawn_local(f);
                                v.push(f);
                            },
                            Some(FsRequest::Jobs(id, project)) => {
                                debug!("Got jobs request");
                                let tx2 = tx.clone();
                                let client2 = client.clone();
                                let f = async move {
                                    debug!("Getting lock...");
                                    //let mut client2 = client2.lock().await;
                                    debug!("Getting jobs...");
                                    let mut client2 = client2.lock().await;
                                    let jobs = client2.get_jobs_async(project).await;
                                    drop(client2);
                                    debug!("Sending job msg...");
                                    let msg = FsResponse::Jobs(id, jobs.to_owned());
                                    tx2.send(msg).await.unwrap();
                                    debug!("Send job msg");
                                };
                                let f = task::spawn_local(f);
                                v.push(f);
                            },
                            Some(FsRequest::JobsMany(id, projects)) => {
                                debug!("Got jobs many request");
                                let tx2 = tx.clone();
                                let client2 = client.clone();
                                let f = async move {
                                    debug!("Getting lock...");
                                    //let mut client2 = client2.lock().await;
                                    debug!("Getting jobs...");
                                    let mut client2 = client2.lock().await;
                                    let jobs = client2.get_many_jobs_async(projects).await;
                                    drop(client2);
                                    debug!("Sending job msg...");
                                    let msg = FsResponse::JobsMany(id, jobs.to_owned());
                                    tx2.send(msg).await.unwrap();
                                    debug!("Send job msg");
                                };
                                let f = task::spawn_local(f);
                                v.push(f);
                            },
                            Some(FsRequest::Artifact(id, jobdata)) => {
                                debug!("Got artifact request");
                                let tx2 = tx.clone();
                                let client2 = client.clone();
                                let f = async move {
                                    debug!("Getting lock...");
                                    //let mut client2 = client2.lock().await;
                                    debug!("Getting artifact...");
                                    let mut client2 = client2.lock().await;
                                    let b = client2.get_artifact_from_job_id_async(jobdata.project_id, jobdata.job_id).await;
                                    drop(client2);
                                    debug!("Sending artifact msg...");
                                    let msg = FsResponse::Artifact(id, b.to_owned());
                                    tx2.send(msg).await.unwrap();
                                    debug!("Sending artifact msg...");
                                };
                                let f = task::spawn_local(f);
                                v.push(f);
                            },
                        }
                    },
                };
            }
        }).await;
    });
}
