use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabJobArtifactsFileModel {
    pub filename: String,
    pub size: u64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabJobPipelineModel {
    pub id: u64,
    // Rename the key because ref is a keyword
    #[serde(rename = "ref")]  
    pub _ref: String,
    pub sha: String,
    pub status: String,
}
    

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabJobModel {
    //commit: {
    //  "author_email": "admin@example.com",
    //  "author_name": "Administrator",
    //  "created_at": "2015-12-24T16:51:14.000+01:00",
    //  "id": "0ff3ae198f8601a285adcf5c0fff204ee6fba5fd",
    //  "message": "Test the CI integration.",
    //  "short_id": "0ff3ae19",
    //  "title": "Test the CI integration."
    //},
    //"coverage": null,
    //"allow_failure": false,
    pub created_at: String,
    pub started_at: String,
    pub finished_at: String,
    //"duration": 0.173,
    pub artifacts_file: Option<GitlabJobArtifactsFileModel>,
    //"artifacts": [
    //  {"file_type": "archive", "size": 1000, "filename": "artifacts.zip", "file_format": "zip"},
    //  {"file_type": "metadata", "size": 186, "filename": "metadata.gz", "file_format": "gzip"},
    //  {"file_type": "trace", "size": 1500, "filename": "job.log", "file_format": "raw"},
    //  {"file_type": "junit", "size": 750, "filename": "junit.xml.gz", "file_format": "gzip"}
    //],
    //"artifacts_expire_at": "2016-01-23T17:54:27.895Z",
    pub id: u64,
    pub name: String,
    pub pipeline: GitlabJobPipelineModel,

    // Rename the key because ref is a keyword
    #[serde(rename = "ref")]  
    pub _ref: String,

    //"artifacts": [],
    //"runner": null,
    //"stage": "test",
    pub status: String,
    //"tag": false,
    //"web_url": "https://example.com/foo/bar/-/jobs/7",
    //"user": {
    //  "id": 1,
    //  "name": "Administrator",
    //  "username": "root",
    //  "state": "active",
    //  "avatar_url": "http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?s=80&d=identicon",
    //  "web_url": "http://gitlab.dev/root",
    //  "created_at": "2015-12-21T13:14:24.077Z",
    //  "bio": null,
    //  "location": null,
    //  "public_email": "",
    //  "skype": "",
    //  "linkedin": "",
    //  "twitter": "",
    //  "website_url": "",
    //  "organization": ""
    //}
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabGroupModel {
    pub id: i64,
    pub name: String,
    pub path: String,
    //description: String,
    //visibility: String,
    //share_with_group_lock: bool,
    //require_two_factor_authentication: bool,
    //two_factor_grace_period: i32,
    //project_creation_level: String,
    //auto_devops_enabled: bool,
    //subgroup_creation_level: String,
    //emails_disabled: bool,
    //mentions_disabled: bool,
    //lfs_enabled: bool,
    //default_branch_protection: i32,
    //avatar_url: String,
    pub web_url: String,
    //request_access_enabled: bool,
    pub full_name: String,
    pub full_path: String,
    //file_template_project_id: i32,
    //parent_id: bool,
    pub created_at: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GitlabProjectModel {
    pub id: i64,
    pub description: Option<String>,
    pub default_branch: Option<String>,
    pub ssh_url_to_repo: String,
    pub http_url_to_repo: String,
    pub web_url: String,
    //readme_url: String,
    //tag_list: Vec<String>,
    pub name: String,
    //name_with_namespace: String,
    //path: String,
    pub path_with_namespace: String,
    //created_at: DateTime<Local>,
    //last_activity_at: DateTime<Local>,
    //forks_count: i32,
    //avatar_url: String,
    //star_count: i32,
}

#[derive(Debug, Clone)]
pub struct ZipFile {
    pub path: Option<String>,
    pub data: bytes::Bytes,
}

#[derive(Debug, Clone)]
pub struct GitlabJobProjectData {
    pub job_id: u64,
    pub project_id: i64,
    pub file: Option<ZipFile>,
}


