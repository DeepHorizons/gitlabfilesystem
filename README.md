# Gitlab Filesystem
Mount a virtual filesystem that lets you access gitlab artifacts


## Installing
Releases are available at https://gitlab.com/DeepHorizons/gitlabfilesystem/-/releases

You can install the `.deb` using `dpkg` and `apt`

```bash
dpkg -i gitlabfilesystem*.deb
# dpkg might complain about missing deps, install them with apt
apt install -f
```


## Using
First you need to generate a personal access token from gitlab (https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). You only need api access.

Then mount the filesystem using the binary:

```bash
gitlabfilesystem <personal access token>@<gitlab api url> <mountpoint>
```

for example

```bash
gitlabfilesystem abc123@https://gitlab.com/api/v4 /mnt
```


## Building
You need rust and cargo installed,
in addition to `libssl-dev` and `libfuse-dev`

```bash
apt install libssl-dev libfuse3-dev
cargo build
```

