#!/bin/bash
# Automate installing packages on ubuntu to make my life easier

# Exit on errors
set -e
# Show the command being run
set -x

# Install deps
apt update && apt install -y curl libfuse-dev libssl-dev make build-essential pkg-config
# XXX Older Ubuntu releases dont have libfuse3, so install it conditionally
apt install -y libfuse3-dev || true

# if cargo does not exist, install it
if ! command -v cargo 2>&1 > /dev/null
then
	# Install the latest rust
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
	# Make cargo available immediately
	. $HOME/.cargo/env
fi

# Install the tools needed to make a deb
cargo install cargo-deb

# Make a release
make dist
